
# react-native-device-infos

## Getting started

`$ npm install https://tructmkst@bitbucket.org/tructmkst/deviceinfos.git --save`

### Mostly automatic installation

`$ react-native link react-native-device-infos`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-device-infos` and add `RNDeviceInfos.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNDeviceInfos.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNDeviceInfosPackage;` to the imports at the top of the file
  - Add `new RNDeviceInfosPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-device-infos'
  	project(':react-native-device-infos').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-device-infos/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-device-infos')
  	```

#### Windows
[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNDeviceInfos.sln` in `node_modules/react-native-device-infos/windows/RNDeviceInfos.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app
  - Add `using Device.Infos.RNDeviceInfos;` to the usings at the top of the file
  - Add `new RNDeviceInfosPackage()` to the `List<IReactPackage>` returned by the `Packages` method


## Usage
```javascript
import RNDeviceInfos from 'react-native-device-infos';

console.log(RNDeviceInfos.getBundleId());
console.log(RNDeviceInfos.getManufacturer()); // return 'Apple' or 'Android'
console.log(RNDeviceInfos.getUniqueID());
console.log(RNDeviceInfos.getVersion());
console.log(RNDeviceInfos.getWifiInfo());
```
  