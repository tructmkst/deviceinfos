var RNDeviceInfos = require('react-native').NativeModules.RNDeviceInfos;

module.exports = {
    getManufacturer: function() {
        return RNDeviceInfos.systemManufacturer;
    },
    getVersion: function() {
        return RNDeviceInfos.appVersion;
    },
    getBundleId: function() {
        return RNDeviceInfos.bundleId;
    },
    getUniqueID: function() {
        return RNDeviceInfos.uuid;
    }
};
