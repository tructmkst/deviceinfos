
#import "RNDeviceInfos.h"
#import "UICKeyChainStore.h"

@implementation RNDeviceInfos

RCT_EXPORT_MODULE(RNDeviceInfos)
+ (BOOL)requiresMainQueueSetup
{
   return YES;
}

- (NSString*) uuid
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *uuidUserDefaults = [defaults objectForKey:@"uuid"];
    
    NSString *uuid = [UICKeyChainStore stringForKey:@"uuid"];
    
    if ( uuid && !uuidUserDefaults) {
        [defaults setObject:uuid forKey:@"uuid"];
        [defaults synchronize];
        
    }  else if ( !uuid && !uuidUserDefaults ) {
        NSString *uuidString = [[NSUUID UUID] UUIDString];
        
        [UICKeyChainStore setString:uuidString forKey:@"uuid"];
        
        [defaults setObject:uuidString forKey:@"uuid"];
        [defaults synchronize];
        
        uuid = [UICKeyChainStore stringForKey:@"uuid"];
        
    } else if ( ![uuid isEqualToString:uuidUserDefaults] ) {
        [UICKeyChainStore setString:uuidUserDefaults forKey:@"uuid"];
        uuid = [UICKeyChainStore stringForKey:@"uuid"];
    }
    
    return uuid;
}

- (NSDictionary *)constantsToExport
{
    return @{
             @"uuid": [self uuid],
             @"bundleId": [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleIdentifier"],
             @"appVersion": [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"] ?: [NSNull null],
             @"systemManufacturer": @"Apple"
             };
}

@end
  
